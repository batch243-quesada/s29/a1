db.users.insert({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone: "12345678",
			email: "jandedoe@gmail.com"
		},
		courses: [ "CSS", "JavaScript", "Python"],
		department: "none"
	});

db.users.insertMany([{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses:[ "Python", "React", "PHP"],
		department: "none"
	}, {
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}]);

db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	course: [],
	department: "none"
});


// Find users with letter “s” in their first name or “d” in their last name
db.users.find({$or : [{firstName : {$regex : 's'}}, {lastName : {$regex : 'd'}}]}, {
	firstName: true,
	lastName: true,
	_id: false
}).pretty();

// Find users who are from the HR department and their age is greater than or equal to 70.
db.users.find({$and : [{department : "HR"}, {age : {$gte : 70}}]}).pretty();

// Find users with the letter “e” in their first name and has an age of less than or equal to 30.
// Use the $and, $regex and $lte operators
db.users.find({$and : [{firstName : {$regex : 'e'}}, {age : {$lte : 30}}]}).pretty();